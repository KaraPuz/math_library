#include <iostream>
#include <cassert>
#include "matrix.hpp"

using kr::Mat23i;
using kr::Mat23d;
using kr::Mat32i;
using kr::Mat32d;
using kr::Vec2i;
using kr::Vec2d;
using kr::Vec3i;
using kr::Vec3d;
using kr::Mat22i;
using kr::Mat22d;
using kr::Mat33i;
using kr::Mat33d;
using kr::Mat34i;
using kr::Mat34d;
using kr::Mat44i;
using kr::Mat44d;
using kr::Mat45d;


int main()
{
	
	//������� �������
	Mat22d A;
	std::cin >> A;
	std::cout << A << std::endl;
	std::cout << A.Inverse() << std::endl;
	system("pause");
	//


	//������������
	Mat34d L;
	std::cin >> L;
	std::cout << L << std::endl;
	std::cout << L.Determinant() << std::endl;
	system("pause");
	//


	//�������� �������
	Mat44d B;
	std::cout << B.Unit_Matrix() << std::endl;
	system("pause");
	//


	//��������� �� ����� (����� ��� ����� ���������� ����� �� ����� ��������)
	Mat22i C;
	int num;
	std::cin >> C;
	std::cin >>  num;
	std::cout << C << std::endl;
	std::cout << num << std::endl;
	std::cout << C*num << std::endl;
	system("pause");
	//std::cout << C*5 << std::endl;
	//

	//��������� ������(�� ������ ����������)
	Vec3i D;
	Mat33i E;
	std::cin >> D;
	std::cout << D << std::endl;
	std::cin >> E;
	std::cout << E << std::endl;
	std::cout <<  E*D  << std::endl;
	system("pause");
	//



	//���������������� �������
	Mat22d J;
	std::cin >> J;
	std::cout << J << std::endl;
	std::cout << J.Transposed() << std::endl;
	system("pause");
	//



	//������� ���� ������� ��������//��� ������� ����� ������������ Determinanttt()
	Mat34d Z;//������� 3�4,������ ��� 4�� ������� ������ �������� ��������� ������
	std::cin >> Z;
	std::cout << Z << std::endl;
	std::cout << Z.SLAU() << std::endl;
	system("pause");
	//


	return 0;
}