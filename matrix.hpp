#pragma once
#include <iostream>

namespace kr
{
#define MY_DEBUG //если эту строчку комментируем,то MY_DEBUG автоматически становится неопределенный и тогда выреается строчка std::cout << "Constructor" << std::endl;


	template<typename T, int N, int M>
	struct TotMas
	{
		T mas[N][M];
	};


	template<typename T, int P, int Q>
	struct TotMas2
	{
		T mas[P][Q];
	};

	template<typename T, int N, int M>
	class Matrix
	{

	public:
		// Конструктор
		Matrix()
		{
#ifdef MY_DEBUG//если MY_DEBUG определена,то отладочный вывод выводится
			std::cout << "Constructor" << std::endl;
#endif  //все,что начинается с решеток называется директивами компилятора
			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = 0;
		}

		Matrix(const T mas[N][M])
		{
#ifdef MY_DEBUG
			std::cout << "Constructor" << std::endl;
#endif
			m_n = N;//строка
			m_m = M;//столбец
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mas[i][j];
		}

		Matrix(const TotMas<T, N, M>& mas)
		{
#ifdef MY_DEBUG
			std::cout << "Constructor" << std::endl;
#endif
			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mas.mas[i][j];
		}

		// Конструктор копирования
		Matrix(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG

			std::cout << "Copy constructor" << std::endl;
#endif // MY_DEBUG

			m_n = mat.m_n;
			m_m = mat.m_m;

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mat.m_mat[i][j];
		}

		int getN() const { return m_n; }
		int getM() const { return m_m; }
		T get(int i, int j) const { return m_mat[i][j]; }
		void set(int i, int j, T data) { m_mat[i][j] = data; }

		// Присваивание
		template<typename T, int N, int M>
		Matrix <T, N, M>& operator=(const Matrix<T, N, M>& mat)
		{
			std::cout << "Operator =" << std::endl;

			m_n = mat.getN();
			m_m = mat.getM();

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mat.get(i, j);

			return *this;
		}

		// Оператор сложения
		template<typename T, int N, int M>
		Matrix <T, N, M> operator+(const Matrix<T, N, M>& mat)
		{
			std::cout << "operator+" << std::endl;
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
			return tmp;
		}


		// Оператор += 
		template<typename T, int N, int M>
		Matrix <T, N, M> operator+=(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "operator+=" << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] + mat.get(i, j);
			return tmp;
		}


		// Оператор умножения на число 
		Matrix <T, N, M> operator*(T num)
		{
#ifdef MY_DEBUG
			std::cout << "operator * na chiclo  " << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] * num;
			return tmp;
		}




		// Оператор умножения
		template<typename T, int P, int Q>
		auto operator*(const Matrix<T, P, Q>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "operator*" << std::endl;
#endif
			Matrix<T, N, Q> tmp;
			if (M == P)
			{
				for (int i = 0; i < m_n; i++)
					for (int j = 0; j < mat.getM(); j++)
					{
						T sum = 0;
						for (int k = 0; k < m_m; k++)
							sum += m_mat[i][k] * mat.get(k, j);
						tmp.set(i, j, sum);
					}
				return tmp;
			}
			else
			{
				std::cout << "enter correctly" << std::endl;
			}
		}




		//оператор вычитания
		template<typename T, int N, int M>
		Matrix <T, N, M> operator-(const Matrix<T, N, M>& mat)
		{
#ifdef MY_DEBUG
			std::cout << "operator-" << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
			return tmp;
		}


		//определитель
		T Determinant()
		{

			if (m_n == m_m)
			{
				T kst;
				T p = 0;

				for (int i = 0; i < m_n - 1; i++)
				{
					int t = 1;
					while (m_mat[i][i] == 0)
					{
						for (int j = 0; j < m_m; j++)
						{
							m_mat[i][j] = kst;
							m_mat[i][j] = m_mat[i + t][j];
							m_mat[i + t][j] = kst;
						}
						p++;
						t++;
					}

					for (int k = i + 1; k < m_n; k++)
					{
						kst = m_mat[k][i] / m_mat[i][i];
						for (int j = 0; j < m_n; j++)
							m_mat[k][j] -= m_mat[i][j] * kst;
					}
				}

				kst = pow(-1, p);
				for (int i = 0; i < m_n; i++)
					kst *= m_mat[i][i];
				return kst;
			}

			if (m_n != m_m)
			{
				std::cout << "error" << std::endl;
				return -1;
			}
		}


		





		//решение слау методом Краммера
		Matrix <T, N, 1> SLAU()
		{

#ifdef MY_DEBUG
			std::cout << "SLAU matrix" << std::endl;
#endif


			T d = Determinanttt();
			if (d == 0)
			{
				std::cout << "impossible,because Determinant=0 " << std::endl;
			}

			Matrix<T, N, 1> RECHENIE;

			if ((m_m - 1) == m_n)
			{

				for (int swapJ = 0; swapJ < m_m - 1; swapJ++)//swapJ-столбец подмены
				
				{
					Matrix<T, N, M> newMatrix = m_mat;
					for (int i = 0; i < m_n; i++)
					{
						for (int j = 0; j < m_m - 1; j++)
						{
							if (swapJ == j) {
								newMatrix.m_mat[i][j] = m_mat[i][m_m - 1];
							}

						}

					}
					RECHENIE.set(swapJ, 0, newMatrix.Determinanttt() / d);

				}
				return RECHENIE;
			}
			if ((m_m - 1) != m_n)
			{
				std::cout << "error,enter again" << std::endl;
			    
			}
		}


		//обратная матрица
		Matrix <T, N, M> Inverse()
		{

#ifdef MY_DEBUG
			std::cout << "inverse matrix" << std::endl;
#endif

			T d = Determinant();
			if (d == 0)
			{
				std::cout << "impossible,because Determinant=0 " << std::endl;
			}
			else {
				if ((m_n == 2 && m_m == 2) || (d != 0))
				{
					Matrix<T, N, M> tmp;
					T f;
					f = m_mat[0][0] * m_mat[1][1] - m_mat[0][1] * m_mat[1][0];

					tmp.m_mat[0][0] = m_mat[1][1] / f;
					tmp.m_mat[1][0] = (-m_mat[1][0]) / f;
					tmp.m_mat[0][1] = (-m_mat[0][1]) / f;
					tmp.m_mat[1][1] = m_mat[0][0] / f;

					return tmp;
				}

				if ((m_n == 3 && m_m == 3) || (d != 0))
				{
					Matrix<T, N, M> tmp;
					T k;
					k = tmp.Determinant();

					tmp.m_mat[0][0] = (m_mat[1][1] * m_mat[2][2] - m_mat[2][1] * m_mat[1][2]) / k;
					tmp.m_mat[0][1] = -(m_mat[0][1] * m_mat[2][2] - m_mat[2][1] * m_mat[0][2]) / k;
					tmp.m_mat[0][2] = (m_mat[0][1] * m_mat[1][2] - m_mat[1][1] * m_mat[0][2]) / k;

					tmp.m_mat[1][0] = -(m_mat[1][0] * m_mat[2][2] - m_mat[2][0] * m_mat[1][2]) / k;
					tmp.m_mat[1][1] = (m_mat[0][0] * m_mat[2][2] - m_mat[2][0] * m_mat[0][2]) / k;
					tmp.m_mat[1][2] = -(m_mat[0][0] * m_mat[1][2] - m_mat[1][0] * m_mat[0][2]) / k;

					tmp.m_mat[2][0] = (m_mat[1][0] * m_mat[2][1] - m_mat[2][0] * m_mat[1][1]) / k;
					tmp.m_mat[2][1] = -(m_mat[0][0] * m_mat[2][1] - m_mat[2][0] * m_mat[0][1]) / k;
					tmp.m_mat[2][2] = (m_mat[0][0] * m_mat[1][1] - m_mat[1][0] * m_mat[0][1]) / k;

					return tmp;
				}
			}

		}

		//транспонированная матрица
		Matrix <T, N, M> Transposed()
		{
#ifdef MY_DEBUG
			std::cout << "transposed matrix" << std::endl;
#endif
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
				{
					tmp.m_mat[i][j] = m_mat[j][i];
				}
			return tmp;
		}



		//создание единичной матрицы 
		Matrix <T, N, M> Unit_Matrix()
		{
#ifdef MY_DEBUG
			std::cout << "the unit matrix" << std::endl;
#endif


			if (m_n == m_m)
			{



				for (int i = 0; i < m_n; i++)
				{
					for (int j = 0; j < m_m; j++)
					{
						if (i == j) // Если диагональ 
							m_mat[i][j] = 1;
						else // а если нет
							m_mat[i][j] = 0;




					}

				}
				return m_mat;
			}
			else { std::cout << "error" << std::endl; }
		}








		// Деструктор
		~Matrix()
		{
#ifdef MY_DEBUG
			std::cout << "Destructor" << std::endl;
#endif
		}

		// friend - позволяет функции иметь доступ к private полям/методам класса
		template<typename T, int N, int M>
		friend std::istream& operator>>(std::istream& os, Matrix<T, N, M>& mat);

		template<typename T, int N, int M>
		friend std::ostream& operator<<(std::ostream& os, const Matrix<T, N, M>& mat);



		// Использование внутри класса
	private:
		int m_n, m_m;		// Поле
		T m_mat[N][M];


		//определитель для уравнения
		T Determinanttt()
		{
			Matrix<T, N, M> tmp = m_mat;
			if (m_n == (m_m - 1))
			{
				T kst;
				T p = 0;

				for (int i = 0; i < m_n - 1; i++)
				{
					int t = 1;
					while (tmp.m_mat[i][i] == 0)
					{
						for (int j = 0; j < m_n; j++)
						{
							tmp.m_mat[i][j] = kst;
							tmp.m_mat[i][j] = m_mat[i + t][j];
							tmp.m_mat[i + t][j] = kst;
						}
						p++;
						t++;
					}

					for (int k = i + 1; k < m_n; k++)
					{
						kst = tmp.m_mat[k][i] / tmp.m_mat[i][i];
						for (int j = 0; j < m_n; j++)
							tmp.m_mat[k][j] -= tmp.m_mat[i][j] * kst;
					}
				}

				kst = pow(-1, p);
				for (int i = 0; i < m_n; i++)
					kst *= tmp.m_mat[i][i];
				return kst;
			}

			if (m_n != (m_m - 1))
			{
				std::cout << "error dimen" << m_n << (m_m - 1) << std::endl;
				return -1;
			}
		}

	};

	// Перегрузка оператора ввода
	template<typename T, int N, int M>
	std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat)
	{
		for (int i = 0; i < mat.m_n; i++)
			for (int j = 0; j < mat.m_m; j++)
				in >> mat.m_mat[i][j];
		return in;
	}

	// Перегрузка оператора вывода
	template<typename T, int N, int M>
	std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat)
	{
		out << "Matrix " << mat.m_n << "x" << mat.m_m << std::endl;
		for (int i = 0; i < mat.m_n; i++) {
			for (int j = 0; j < mat.m_m; j++)
				out << mat.m_mat[i][j] << " ";
			out << std::endl;
		}
		return out;
	}

	// Сокращения для просоты использования (строка,столбец)

	using Vec2i = Matrix<int, 2, 1>;
	using Vec2d = Matrix<double, 2, 1>;

	using Vec3i = Matrix<int, 3, 1>;
	using Vec3d = Matrix<double, 3, 1>;

	using Mat22i = Matrix<int, 2, 2>;
	using Mat22d = Matrix<double, 2, 2>;

	using Mat33i = Matrix<int, 3, 3>;
	using Mat33d = Matrix<double, 3, 3>;

	using Mat23i = Matrix<int, 2, 3>;
	using Mat23d = Matrix<double, 2, 3>;

	using Mat32i = Matrix<int, 3, 2>;
	using Mat32d = Matrix<double, 3, 2>;

	using Mat34i = Matrix<int, 3, 4>;
	using Mat34d = Matrix<double, 3, 4>;


	using Mat44i = Matrix<int, 4, 4>;
	using Mat44d = Matrix<double, 4, 4>;
	using Mat45d = Matrix<double, 4, 5>;
}
